from django.db import models

class Actor(models.Model):
	name = models.CharField(max_length=50)
	surname = models.CharField(max_length=50)

class Movie(models.Model):
	title = models.CharField(max_length=50)
	original_title = models.CharField(max_length=50,null=True)
	ext = models.CharField(max_length=10)
	id_imdb = models.CharField(max_length=30,blank=True)
	actor = models.ManyToManyField(Actor,related_name='movieactor',blank=True)
#######data reperibile da wikipedia / omdb############
	img = models.CharField(max_length=255,blank=True)
	img_ext = models.CharField(max_length=10,blank=True)
	year = models.CharField(max_length=15,blank=True)
	plot = models.TextField(blank=True)
	runtime = models.CharField(max_length=15,blank=True)
	disabled = models.BooleanField(default=False)

	#crearea tabella N a N e usare il trought
	#other data about film 

