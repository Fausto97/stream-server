from django.urls import path,re_path
from . import views
from django.conf.urls.static import static
from django.conf import settings


app_name='WatchMovie'
urlpatterns = [
    path('',views.index,name='index'),
    path('movie/<int:movie_id>/',views.stream_movie,name='stream_movie'),
    path('search/',views.search_movie,name="search_movie")
]+ static(settings.MOVIES_URL, document_root=settings.MOVIES_ROOT)
