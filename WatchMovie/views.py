from django.shortcuts import render
from .models import Movie
from .forms import *
from django.conf import settings
from django.db.models import CharField, Value as V
from django.db.models.functions import Concat
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
import os

##### views #####
def index(request):
	form = Search()
	return render(request,'movies/index.html',{'form':form})

def search_movie(request):

	title=request.GET.get('title')
	page = request.GET.get('p')
	if title:
		movies = Movie.objects.filter(title__contains=title,disabled=False).values('id','title','img','runtime')
		paginator = Paginator(movies,8)
		movies = paginator.get_page(page)
		return render(request,'movies/search.html',{'movies':movies,'search':title})

	else:
		movies = Movie.objects.filter(disabled=False).values('id','title','img','runtime')
		paginator = Paginator(movies,8)
		if not page:
			page = 1
		movies = paginator.get_page(page)
	return render(request,'movies/all.html',{'movies':movies, 'search':title})

def stream_movie(request,movie_id):
	film = None
	if movie_id:
		film = Movie.objects.get(id=movie_id)
		title = film.title.replace(" ","_")
		ext = film.ext
		link = os.path.join(settings.MOVIES_URL_STREAM,title+ext)
	return render(request,'movies/stream.html',{'film':film,'link':link})