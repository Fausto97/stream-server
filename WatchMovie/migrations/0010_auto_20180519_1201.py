# Generated by Django 2.0.2 on 2018-05-19 12:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('WatchMovie', '0009_auto_20180519_1149'),
    ]

    operations = [
        migrations.AlterField(
            model_name='movie',
            name='original_title',
            field=models.CharField(max_length=50, null=True),
        ),
    ]
