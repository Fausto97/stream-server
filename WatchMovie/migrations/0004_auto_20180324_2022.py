# Generated by Django 2.0.2 on 2018-03-24 20:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('WatchMovie', '0003_auto_20180324_1333'),
    ]

    operations = [
        migrations.AlterField(
            model_name='movie',
            name='img',
            field=models.CharField(blank=True, default='', max_length=255),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='movie',
            name='second_title',
            field=models.CharField(blank=True, default='', max_length=50),
            preserve_default=False,
        ),
    ]
