# Generated by Django 2.0.2 on 2018-04-14 13:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('WatchMovie', '0004_auto_20180324_2022'),
    ]

    operations = [
        migrations.RenameField(
            model_name='movie',
            old_name='movie',
            new_name='actor',
        ),
        migrations.RemoveField(
            model_name='movie',
            name='second_title_en',
        ),
        migrations.RemoveField(
            model_name='movie',
            name='title_en',
        ),
        migrations.AddField(
            model_name='movie',
            name='runtime',
            field=models.CharField(blank=True, max_length=15),
        ),
        migrations.AddField(
            model_name='movie',
            name='second_imdb',
            field=models.CharField(blank=True, max_length=50),
        ),
        migrations.AddField(
            model_name='movie',
            name='title_imdb',
            field=models.CharField(blank=True, max_length=50),
        ),
        migrations.AlterField(
            model_name='movie',
            name='id_imdb',
            field=models.CharField(blank=True, default=1, max_length=30),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='movie',
            name='img_ext',
            field=models.CharField(blank=True, default=1, max_length=10),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='movie',
            name='plot',
            field=models.TextField(blank=True),
        ),
        migrations.AlterField(
            model_name='movie',
            name='year',
            field=models.CharField(blank=True, max_length=15),
        ),
    ]
