from django.apps import AppConfig


class WatchfilmConfig(AppConfig):
    name = 'WatchFilm'
