import bs4 as bs
from django.conf import settings
from urllib.parse import quote_plus,urljoin
import requests

class ImdbScraping():

	def get_original_title(self,title):

		find_url = urljoin(settings.IMDB_SCRAPING_BASE_URL,'/find')
		#hardcoded params cause of "+" (with "2%B")
		params = '?ref_=nv_sr_fn&q='+quote_plus(title)

		response = requests.get(find_url+params)

		sauce = response.text
		soup = bs.BeautifulSoup(sauce,'lxml')

		title_link = soup.td.a.get('href')

		#get conent of original_title tag
		find_original_title_url = urljoin(settings.IMDB_SCRAPING_BASE_URL,title_link)
		response_original_title = requests.get(find_original_title_url)
		#beautifuly the content of the page
		sauce_original_title = response_original_title.text
		soup_original_title = bs.BeautifulSoup(sauce_original_title,'lxml')
		#filter by the div class : "originalTitle"
		original_title = soup_original_title.find('div',class_='originalTitle')
		#return text ONLY between div tags
		""" if it's none it means that the original title tag does not exists
		so , the inputted title it's already the original title"""
		res = original_title.contents[0] if original_title else None
		
		return res

imdbscraping = ImdbScraping()