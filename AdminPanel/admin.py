from django.contrib import admin
from .models import *
from WatchMovie.models import Movie

def movie_title(obj):
	m = Movie.objects.get(id=obj.id_movie.id)
	return m.title

@admin.register(MoviesLog)
class MoviesLogAdmin(admin.ModelAdmin):
    list_display = (movie_title,'get_action_display')