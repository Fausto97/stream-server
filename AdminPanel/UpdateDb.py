import os
from WatchMovie.models import *
from .api_omdb import ApiImdb
from django.conf import settings
from WatchSeries.models import *
from .logger import AddLog

class UpdateDBMovies():
	def __init__(self):
		self.movies = os.listdir(settings.MOVIES_ROOT)
		self.filename_divider = lambda x :os.path.splitext(x.replace("_"," "))

	
	#dato un oggetto Movie , lo disablilita
	@staticmethod
	def _disable_movie(movie):
		movie.disabled = True
		movie.save()
		AddLog(movie).disabled()

	#dato un oggetto Movie , lo abilita
	@staticmethod
	def _enable_movie(movie):
		movie.disabled = False
		movie.save()
		AddLog(movie).enabled()

	"""
	dati titolo!,estensione!,id_imdb e titolo originale
	li inserisce all'interno del database
	"""
	@staticmethod
	def save_movie_record(title,ext,id_imdb=None,original_title=None):
		movie = Movie(title=title,ext=ext,id_imdb=id_imdb,
			original_title=original_title)
		movie.save()
		AddLog(movie).inserted()

	"""
	selezionati tutti i con id_imdb , aggiunge loro
	le informazioni prese da imdb
	"""
	def add_info_movies(self):
		from django.db.models import Q
		arr = []
		movies = Movie.objects.filter(~Q(id_imdb="")).values('id','id_imdb')

		for i in movies:
			f = Movie.objects.get(id=i['id'])
			if not f.img:
				info = ApiImdb().search_by_id(i['id_imdb'])			
				f.year,f.img,f.plot,f.runtime = info['Year'],info['Poster'],info['Plot'],info['Runtime']
				f.save()
				arr.append({'id_imdb':i['id_imdb']})

	#disabilita il film non più esistenti su memoria
	def disable_movies(self):
		all_the_movies = Movie.objects.filter(disabled=False).values('id','title','ext')
		for i in all_the_movies:
			film = i['title'].replace(" ","_")+i['ext']
			if film not in self.movies:
				m = Movie.objects.get(id=i['id'])
				self._disable_movie(m)

	"""
	dato un titolo di un film , cerca su imdb
	la corrispondenza , se non viene trovata , 
	viene fatto scraping sul sito di id_imdb
	"""
	@staticmethod
	def search_idimdb(film):
		from .web_scraping import imdbscraping
		#cerco su imdb l'id in base al titolo
		id_imdb,original_title = None,None
		search_id_omdb = ApiImdb().search_by_title(film)
		if search_id_omdb:
			id_imdb = search_id_omdb['imdbID']
		#se non esiste , eseguo web scraping
		else:
			search_original_title = imdbscraping.get_original_title(film)
			if search_original_title:
				id_imdb = ApiImdb().search_by_title(search_original_title)
				id_imdb = id_imdb['imdbID'] if id_imdb else None

		ret = {'id_imdb':id_imdb,'original_title':original_title}
		return	ret 

	"""
	se è disabilitato:
		-abilita film
	se non esiste:
		-cerca id_imdb
		-salva il film
	"""
	def insert_movies_and_id_imdb(self):
		for i in self.movies:
			#controllo se non è un file invisibile
			if i[0] != '.':
				film ,ext= self.filename_divider(i)
				"""controllo se il record è già stato inserito nel db ,
				 in caso contrario , lo inserisco"""
				movie = Movie.objects.filter(title=film).first()
				if movie:
					if movie.disabled:
						self._enable_movie(movie)
				else:
					data = self.search_idimdb(film)
					self.save_movie_record(film,ext,data['id_imdb'],data['original_title'])

				self.disable_movies()

	"""
	se è disabilitato:
		-abilita film
	se non esiste:
		-salva il film
	"""
	def insert_movies(self):
		for i in self.movies:
			if i[0] != '.':
				film ,ext= self.filename_divider(i)
				movie = Movie.objects.filter(title=film).first()
				if movie:
					if movie.disabled:
						self._enable_movie(movie)
				else:
					self.save_movie_record(film,ext)




class UpdateDBSeries():
	def upload_series_and_idimdb(self):
		serie_aggiunte=[]
		serie_eliminate=[]
		episodi_aggiunti=[]
		episodi_eliminati=[]
		for root, directories, filenames in os.walk(settings.SERIES_ROOT):
			for filename in filenames:
				if not os.path.isfile(filename):
					#eseguire il controllo se prima è stato inserito
					filename ,ext = os.path.splitext(filename)
					num,title = filename.split("~")

					abs_path = os.path.join(root,filename)
					seas= os.path.basename(os.path.dirname(abs_path))
					series_name = os.path.basename(os.path.dirname(os.path.dirname(abs_path)))
					series = Series.objects.filter(title=series_name)
					if not series:
						id_imdb = ApiImdb().search_by_title(series_name)
						id_imdb = id_imdb['imdbID'] if id_imdb else ""
						series = Series(title=series_name,id_imdb=id_imdb)
						series.save()
						serie_aggiunte.append({'titolo':series_name,'id_imdb':id_imdb})
					else:
						series = Series.objects.get(title=series_name)

					episode = Episode.objects.filter(title=title,series=series)

					if not episode:
						episode = Episode(series=series,num=num,title=title,ext=ext,season=seas)
						episode.save()

						episodi_aggiunti.append({'titolo':title,'episodio':num,'stagione':seas,
							'ext':ext,'serie':series.title})

		all_the_series = Series.objects.all().values('id','title')
		for i in all_the_series:
			if not os.path.exists(os.path.join(settings.SERIES_ROOT,i['title'])):
				series = Series.objects.get(id=i['id']).delete()
				serie_eliminate.append({'titolo':i['title']})

		all_the_episodes = Episode.objects.all().values('id')

		for i in all_the_episodes:
			ep = Episode.objects.get(id=i['id'])
			title=ep.title.replace(" ","_")
			season = str(ep.season)
			filename = str(ep.num)+"~"+title+ep.ext
			path_exists = os.path.exists(os.path.join(settings.SERIES_ROOT,ep.series.title,season,filename))

			if not path_exists:
				episode = Episode.objects.get(id=i['id']).delete()
				episodi_eliminati.append({'titolo':ep.title,'stagione':ep.season})


		data = {'episodi_aggiunti':episodi_aggiunti,
				'serie_aggiunte':serie_aggiunte,
				'episodi_eliminati':episodi_eliminati,
				'serie_eliminate':serie_eliminate} 
		return data

	def add_info_series(self):
		from django.db.models import Q
		arr = []
		series = Series.objects.filter(~Q(id_imdb="")).values('id','id_imdb')

		for i in series:
			s = Series.objects.get(id=i['id'])
			#controllo se il campo immagini è settato , se no
			#non faccio nulla
			if not s.img:
				info = ApiImdb().search_by_id(i['id_imdb'])
				s.year,s.img,s.plot = info['Year'],info['Poster'],info['Plot'],
				s.save()
				arr.append({'id_imdb':i['id_imdb']})

		return {'arr_series':arr}

updatedbmovies = UpdateDBMovies()
updatedbSeries = UpdateDBSeries
