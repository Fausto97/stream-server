from django.shortcuts import render,redirect
from django.http import HttpResponse
from .forms import Login
from django.contrib.auth import authenticate, login as login_auth
from django.contrib.auth.decorators import login_required
from django.conf import settings
import os
import pathlib
from WatchMovie.models import Movie
from .UpdateDb import updatedbmovies


def login_admin(request):
	if request.method == 'POST':
		form = Login(request.POST)
		if form.is_valid():
			username = form.cleaned_data['username']
			password = form.cleaned_data['password']
			user = authenticate(request,username=username,password=password)
			if user is not None:
				login_auth(request,user)
				return redirect('AdminPanel:main_admin')
			else:
				form = Login()
				msg = "Errore nell'autenticazione : Username e/o password sbagliati"
				return render(request, 'AdminPanel/login.html',{'form':form,'msg':msg})

		else:
			form = Login()
			msg = "Form non valido"
			return render(request, 'AdminPanel/login.html',{'form':form,'msg':msg})

	else:
		if request.user.is_authenticated:
			return render(request,'AdminPanel/main_admin.html')
		form = Login()
	return render(request, 'AdminPanel/login.html', {'form': form})

@login_required
def main_admin(request):
	return render(request,'AdminPanel/main_admin.html')

@login_required
def upload_movies(request):
	res = updatedbmovies.insert_movies_and_id_imdb()
	return render(request, 'AdminPanel/main_admin.html',res)

@login_required
def upload_series(request):
	res = updatedb.upload_series()
	return render(request, 'AdminPanel/main_admin.html',res)

@login_required
def add_info_movies(request):
	res = updatedbmovies.add_info_movies()
	return render(request,'AdminPanel/main_admin.html',res)

@login_required
def add_info_series(request):
	res = updatedb.add_info_series()
	return render(request,'AdminPanel/main_admin.html',res)