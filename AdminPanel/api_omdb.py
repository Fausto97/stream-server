import requests
from django.conf import settings

class ApiImdb():


	def search_by_title(self,title):
		payload = {'apikey':settings.API_KEY,'t':title}
		return _return_data(payload)

	def search_by_id(self,id):
		payload = {'apikey':settings.API_KEY,'i':id}
		return _return_data(payload)


def _return_data(payload):
	r = requests.get(settings.OMDB_URL , params=payload)
	res = r.json()
	if not r.status_code == requests.codes.ok:
		raise Exception("Errore durante la ricerca del id")

	if res['Response'] == "False":
		return None
	else :
		return res


