from django.db import models
from WatchMovie.models import Movie

DISABLE = 0
INSERT = 1
ENABLE = 2

ACTIONS = [[DISABLE,'Disabilitato'],
		[INSERT,'Inserito'],
		[ENABLE,'Abilitato']]

class MoviesLog(models.Model):
	
	id_movie = models.ForeignKey(Movie,on_delete=models.CASCADE)
	action = models.IntegerField(choices=ACTIONS)

