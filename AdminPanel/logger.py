from .models import *

class AddLog():

	def __init__(self,id_movie):
		self.m = MoviesLog(id_movie=id_movie)

	def _save(self,action):
		try:
			self.m.action = action
			self.m.save()
			return True
		except:
			return False

	def disabled(self):
		self._save(DISABLE)

	def inserted(self):
		self._save(INSERT)

	def enabled(self):
		self._save(ENABLE)