from django.urls import path , include
from . import views
app_name="AdminPanel"
urlpatterns = [
	path('',views.login_admin,name='login_admin'),
	path('main/',views.main_admin,name='main_admin'),
	path('upload-movies/',views.upload_movies,name='upload_movies'),
	path('upload-series/',views.upload_series,name='upload_series'),
	path('add-info-series/',views.add_info_series,name='add_info_series'),
	path('add-info-movies/',views.add_info_movies,name='add_info_movies')
]