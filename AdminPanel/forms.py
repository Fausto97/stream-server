from django import forms

class Login(forms.Form):

	widgets = {
	'password': forms.PasswordInput(),	
	}
	username = forms.CharField(label='Nome utente')
	password = forms.CharField(label='Password' , widget=forms.PasswordInput)