from django.urls import path,re_path
from . import views
from django.conf.urls.static import static
from django.conf import settings


app_name='WatchSeries'
urlpatterns = [
    path('',views.series,name='series'),
    path('episodes/<int:id_series>',views.episodes,name='episodes'),
    path('stream/<int:id_ep>',views.stream_series,name='stream_series'),
]+ static(settings.SERIES_URL, document_root=settings.SERIES_ROOT)
