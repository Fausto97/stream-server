from django.shortcuts import render,reverse
from .models import *
from django.core.paginator import Paginator
import os
from django.conf import settings

def series(request):
	page = request.GET.get('p')
	if not page:
		page=1

	series = Series.objects.all().values('id','title','img','year')
	paginator = Paginator(series,5)
	series_paginated = paginator.get_page(page)

	return render(request,'series/series.html',{'series':series_paginated})

def episodes(request,id_series):
	series = Series.objects.get(id=id_series)
	seasons = Episode.objects.filter(series=series).values('season').order_by('season').distinct()
	arr=[]
	print(seasons)
	for i in seasons:
		episodes = (Episode.objects.filter(series=series,season=i['season']).order_by('num')
		.values('id','title','num','season'))
		arr.append([{'stagione':i['season'],'episodi':episodes}])
	print(arr)
	return render(request,'series/episodes.html',{'seasons':arr})

def stream_series(request,id_ep):
	ep = Episode.objects.get(id=id_ep)
	title=ep.title.replace(" ","_")
	filename = ep.series.title+"/"+str(ep.season)+'/'+str(ep.num)+"~"+title+ep.ext
	link = os.path.join(reverse('WatchSeries:series'),settings.SERIES_URL,filename)
	return render(request,'series/stream_series.html',{'episode':ep,'link':link})