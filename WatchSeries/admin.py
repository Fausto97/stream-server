from django.contrib import admin
from .models import *

admin.site.register(Series)
admin.site.register(Episode)