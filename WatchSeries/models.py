from django.db import models
from WatchMovie.models import Actor

class Series(models.Model):
	title = models.CharField(max_length=50)
	title_imdb = models.CharField(max_length=50,blank=True)
	id_imdb = models.CharField(max_length=30,blank=True)
	img = models.CharField(max_length=50,blank=True)
	year = models.CharField(max_length=15,blank=True)
	plot = models.TextField(blank=True)
	actor = models.ManyToManyField(Actor,related_name='seriesactor')

class Episode(models.Model):
	num = models.IntegerField()
	title = models.CharField(max_length=70)
	ext = models.CharField(max_length=10)
	season = models.IntegerField()
	series = models.ForeignKey(Series,on_delete=models.CASCADE)
