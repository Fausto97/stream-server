from django.apps import AppConfig


class WatchseriesConfig(AppConfig):
    name = 'WatchSeries'
