# Generated by Django 2.0.2 on 2018-04-07 10:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('WatchSeries', '0006_auto_20180407_1053'),
    ]

    operations = [
        migrations.AlterField(
            model_name='series',
            name='plot',
            field=models.TextField(blank=True),
        ),
    ]
