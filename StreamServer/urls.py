from django.contrib import admin
from django.urls import path , include
urlpatterns = [
    path('admin/', admin.site.urls),
    path('',include('WatchMovie.urls')),
    path('admin-panel/',include('AdminPanel.urls')),
    path('series/',include('WatchSeries.urls'))
]
